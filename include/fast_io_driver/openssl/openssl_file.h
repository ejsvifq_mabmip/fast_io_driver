#pragma once

namespace fast_io::driver
{

template<std::integral ch_type>
class basic_openssl_io_observer
{
public:
	using char_type = ch_type;
	using native_handle_type = SSLHandle*;
	native_handle_type ssl_handle{};
};

template<std::integral ch_type>
class basic_openssl_file:public basic_openssl_io_observer<ch_type>
{
public:
	
};

/*
template<std::integral ch_type,std::contiguous_iterator Iter>
inline Iter read(basic_openssl_io_observer<ch_type> iob,Iter begin,Iter end)
{

}

template<std::integral ch_type,std::contiguous_iterator Iter>
inline Iter write(basic_openssl_io_observer<ch_type> iob,Iter begin,Iter end)
{

}
*/

}