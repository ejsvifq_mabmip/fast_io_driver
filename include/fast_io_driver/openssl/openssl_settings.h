#pragma once

namespace fast_io::driver::openssl
{

class settings_observer
{
public:
	using native_handle_type = OPENSSL_INIT_SETTINGS*;
	native_handle_type settings_ptr{};
	constexpr operator bool() noexcept
	{
		return settings_ptr;
	}
	auto& native_handle() const noexcept
	{
		return settings_ptr;
	}
	auto& native_handle() noexcept
	{
		return settings_ptr;
	}
};

class settings:public settings_observer
{
public:
	using native_handle_type = OPENSSL_INIT_SETTINGS*;
	constexpr settings(native_handle_type handle):settings_observer(handle){}
	settings():settings_observer(OPENSSL_INIT_new()){}
	settings(settings const&) = delete;
	settings& operator=(settings const&) = delete;

	constexpr settings(settings&& bmv) noexcept:settings_observer(bmv.handle)
	{
		bmv.handle=nullptr;
	}
	settings& operator=(settings&& bmv) noexcept
	{
		if(bmv.native_handle()==this->native_handle())
			return *this;
		if(this->native_handle())[[likely]]
			OPENSSL_INIT_free(this->native_handle());
		this->native_handle()=bmv->native_handle();
		bmv->native_handle()=nullptr;
		return *this;
	}
	constexpr auto release() noexcept
	{
		auto temp{this->native_handle()};
		this->native_handle()=nullptr;
		return temp;
	}
	~settings()
	{
		if(this->native_handle())[[likely]]
			OPENSSL_INIT_free(this->native_handle());
	}
};

};